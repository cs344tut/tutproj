import java.io.*;
public class Authenticate {

    public static void Login() {
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
        String passwd = null;
        String user = null;
        System.out.println("Enter Username:");
        try {
            user = inputReader.readLine();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Enter Password:");
        try {
            passwd = inputReader.readLine();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        StaffMember user_ref;
        user_ref = DataBase.staffMembers.get(user);
        if (user_ref == null) {
            System.out.println("Invalid Credentials");
            System.exit(1);
        } else if (user_ref.getStaffMemberInfo().getPassword().equals(passwd)) {
            if (user_ref.getStaffMemberInfo().getIsScheduler()) {
                SchedulerInterface.mainMenu();
            } else {
                NotificationInterface.driverData((BusDriver) user_ref);
            }
        } else {
            System.out.println("Invalid Credential");
            System.exit(1);
        }
    }
    
    public static void main (String [] args) {
        DataBase.staffMembers.put("jan", new SchedulerEnt(new AuthInfo("jan", "12345", true), "123456"));
        BusDriver john = new BusDriver(new AuthInfo("John", "12345", false), "1234567", "1234567");
        DataBase.staffMembers.put("john", john);
        DataBase.tours.put(999, new Tour("Cape Town", "Joburg", false, john));
        Login();
    }

}
