import java.io.*;

public class SchedulerInterface {

    /* mainMenu constants */
    private static final int CREATE_TOUR = 1;
    private static final int ADD_CUSTOMER = 2;
    private static final int EXIT = 3;
    
    /* addCustomer constants */
    private static final char AC_BACK = 'b';

    public static void mainMenu() {
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
        String command = "";
        final int createTour, addCustomer, addScheduler;
        
        while (true) {
            printMainMenuList();
            try {
                command = inputReader.readLine();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            int commandInt = -1;
            try {
                commandInt = Integer.parseInt(command);    
            } catch (NumberFormatException nfe) {
                System.err.println("Invalid selection!");
            }
            
            switch (commandInt) {
                case CREATE_TOUR:
                    createTourMenu();
                    break;
                case ADD_CUSTOMER:
                    addCustomerMenu();
                    break;
                case EXIT:
                    return;
                default:
                    System.out.println("Invalid selection!");            
            }
        }
    }
    
    public static void printMainMenuList() {
        System.out.println("Select option:");
        System.out.println(CREATE_TOUR + ") Create Tour");
        System.out.println(ADD_CUSTOMER + ") Add customer");
        System.out.println(EXIT + ") Exit");
    }
    
    public static void createTourMenu() {
        while (true) {        
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
            String tourInfo = "";
            System.out.println("Please enter tour information in the format <0|1: public | private;start;destination;bus_driver_username>  or \'b\' for back:");
            while (true) {
                try {
                    tourInfo = inputReader.readLine();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                if (tourInfo.equals("b")) {
                    return;
                } else {
                    addTour(tourInfo);
                    return;
                }
            
            }        
        }    
    }
    
    public static void addCustomerMenu() {
        while (true) {        
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
            String idNumber = "";
            System.out.println("Please enter customer id  or \'b\' for back:");
            while (true) {
                try {
                    idNumber = inputReader.readLine();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                if (idNumber.equals("b")) {
                    return;
                } else {
                    addCustomer(idNumber);
                    return;
                }
            
            }        
        }
    }
    
    public static void addTour(String tourInfo) {
        String parameters[] = tourInfo.split(";");
        if (parameters.length != 4) {
            System.out.println("Invalid format");
        }
        boolean privateTour = Integer.parseInt(parameters[0]) == 0 ? false : true;
        String start = parameters[1];
        String destination = parameters[2];
        String busDriverId = parameters[3];    
        System.out.println(busDriverId);    
        BusDriver driver = (BusDriver) DataBase.staffMembers.get(busDriverId);
        if (driver == null) {
            System.out.println("Bus driver not found!");
        } else {
            DataBase.tours.put(DataBase.tours.size() + 1, new Tour(start, destination, privateTour, driver));
        }
        
        
    }
    
    public static void addCustomer(String idNumber) {
        if (DataBase.customers.get(idNumber) != null) {
            System.out.println("Customer is already in Database");
            addCustomerMenu();
        } else {
            DataBase.customers.put(idNumber, new Customer(idNumber));
        }
    }

    public static void main (String [] args) {
            mainMenu();
    }
}
