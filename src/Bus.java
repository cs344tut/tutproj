public class Bus {
    private String numberPlate;
    private int numSeats;

    public Bus (String numPlate, int seatCap) {
        numberPlate = numPlate;
        numSeats = seatCap;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public int getNumSeats () {
        return numSeats;
    }
}
