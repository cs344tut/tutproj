
public class AuthInfo {
	private String username;
	public String getUsername(){return username;}
	private String password;
	public String getPassword() {return password;}
	private boolean isScheduler;
	public boolean getIsScheduler() {return isScheduler;}
	
	public AuthInfo(String username, String password, boolean isScheduler) {
        	this.username = username;
        	this.password = password;
        	this.isScheduler = isScheduler;
	}
}
