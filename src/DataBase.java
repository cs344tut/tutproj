/* This isn't part of the model, because it simulates the database that will be used. */
import java.util.HashMap;
public class DataBase {

    public static final HashMap<String, StaffMember> staffMembers = new HashMap<String, StaffMember>();
    public static final HashMap<String, Customer> customers = new HashMap<String, Customer>();
    
    public static final HashMap<Integer, Tour> tours = new HashMap<Integer, Tour>();
    
}
