
public class BusDriver extends StaffMember {
	private String idNumber;
	public String getIdNumber() {return idNumber;}
	private String licenseNumber;
	public String getLicenseNumber() {return licenseNumber;}
	
	public BusDriver(AuthInfo ai, String idNumber, String licenseNumber) {
	    super(ai);
	    this.idNumber = idNumber;
	    this.licenseNumber = licenseNumber;
	}
}
