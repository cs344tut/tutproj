
public class Seat {
    private int aisle;
    private int row;

    public Seat(int a, int r) {
        aisle = a;
        row = r;
    }
    public int getAisle () {
        return aisle;
    }
    public int getRow() {
        return row;
    }
}
