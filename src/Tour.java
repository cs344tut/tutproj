
public class Tour {

    private String start;
    private String destination;
    private BusDriver driverRef;
    boolean privateTour;

    public Tour (String st, String Dest, boolean isPrivate, BusDriver localRef) {
        start = st;
        destination = Dest;
        privateTour = isPrivate;
        driverRef = localRef;
    }
    public String getTourStart(){
        return start;
    }
    public String getTourDest(){
        return destination;
    }
    public boolean isPrivateTour(){
        return privateTour;
    }
    public BusDriver getBusDriver(){
        return driverRef;
    }
}
