import java.util.*;
public class Repair {
    Calendar repairDate;

    public Repair (int day, int month, int year) {
        repairDate =  new GregorianCalendar(year, month, day);
    }
    public Calendar getDate(){
        return repairDate;
    }

}
