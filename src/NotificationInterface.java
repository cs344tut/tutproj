import java.util.*;
public class NotificationInterface {
    public static void driverData (BusDriver ref) {
        int i = 1;
        String busDriverID = ref.getIdNumber();
        Iterator it = DataBase.tours.entrySet().iterator();
        for (Tour tmpTour : DataBase.tours.values()) {
            if (tmpTour.getBusDriver().getIdNumber()==busDriverID){
                if (tmpTour.isPrivateTour())
                    System.out.println("Job "+ i + "\t"+"Private:\tYes\tStart Location: "+tmpTour.getTourStart()+"\tDestination Location: "+tmpTour.getTourDest());
                else System.out.println("Job "+ i + "\t"+"Private:\tNo\tStart Location: "+tmpTour.getTourStart()+"\tDestination Location: "+tmpTour.getTourDest());
                i++;
            }
        }
    }
}
